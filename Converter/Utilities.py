def WriteRDFPrefixes(dest_file):
    ''''
    Function which writes a set of RDF prefixes in an output file \
        (typically at the begining of a Turtle file)

    dest_file (str) : name of the file to write
    return: None
    '''
    dest_file.write("""@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix bp3: <http://www.biopax.org/release/biopax-level3.owl#> .
@prefix ctd: <http://ctdbase.org/CTD/> .
@prefix gene: <http://ctdbase.org/gene/> .
@prefix mesh: <http://purl.bioontology.org/ontology/MESH/> .
@prefix sio: <http://semanticscience.org/resource/SIO_> .
@prefix so: <http://purl.obolibrary.org/obo/SO_> .
@prefix taxon:<http://identifiers.org/taxonomy/> .
@prefix organism: <http://eulersharp.sourceforge.net/2003/03swap/organism#>.
@prefix pubmed: <http://bio2rdf.org/pubmed_vocabulary:> .
@prefix cas: <https://www.cas.org/fr/cas-data/cas-registry> .
""")

def GetVariables(current_line:dict)->set:
    '''
    Function which get the values of each column of a tsv file \
        and puts it in variables
    
    current_line (dict) : the line of the file we are reading
    reuturn (set) : a set of variables containing the values for each \
        column of the tsv file for the current line
    '''
    return(current_line['ChemicalID'], current_line['ChemicalName'], current_line['CasRN'],\
         current_line['GeneID'], current_line['GeneForms'], current_line['GeneSymbol'], current_line['Organism'], \
            current_line['OrganismID'], current_line['Interaction'], current_line['InteractionActions'], current_line['PubMedIDs'])

def WriteDicoIDs(dico_file:str, dico:dict)->None:
    '''
    Function which writes the content of a dictionary in an output file

    dico_file (str) : name if the output file
    dico (dct) : the dictionary we want to write in the output file
    
    return : None
    '''
    with open(dico_file, 'w') as fh:
        fh.write("''' \nDictionnary of interactions and IDs \nFor a specific line, Interaction name and ID, and the following IDs for the following reactions and products\n''' \n" + "\n".join([str(items) for items in dico.items()]))