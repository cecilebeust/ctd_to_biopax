"""
CTD to BioPAX Python converter

input : tsv file downloaded from the Comparative Toxicogenomics Database describing chemical-gene interactions
output : ttl file describing these chemical-genes interactions in a RDF format using the BioPAX language

"""

#! /usr/bin/env python3
# Importation of modules
from argparse import ArgumentParser
from csv import DictReader
from pathlib import PurePath
from  uuid import uuid4
from time import time
from re import split
import uuid
from DescribeInteractions import DescribeInteractions
from Utilities import WriteRDFPrefixes
from DescribeEntities import DescribeEntities
from Utilities import GetVariables
from Utilities import WriteDicoIDs

if __name__ == "__main__":
	# Pass the name of the file as argument 
	parser = ArgumentParser()
	parser.add_argument("file", help="the name of the input file", type=str)
	args = parser.parse_args()
	filename = args.file

	# Opening and browsing of the tsv file
	name_for_output:str = str(filename)[:-4]

	# Timer
	StartTime = time()

	with open(PurePath(filename)) as source_file, open(PurePath(f"{name_for_output}.ttl"), 'w') as dest_file:
		# Write the prefixes in the dest file
		WriteRDFPrefixes(dest_file)
		
		# Opening and browsing of the tsv file
		tsv_file = DictReader(filter(lambda row: row[0]!='#', source_file), delimiter="\t", fieldnames=['ChemicalName', 'ChemicalID', 'CasRN', 'GeneSymbol', 'GeneID', 'GeneForms', 'Organism', 'OrganismID', 'Interaction', 'InteractionActions', 'PubMedIDs'])
		DicoIds:dict = {} # A dictionary that will contain the entities/interactions and their IDs
		
		#Provenance = CTD 
		ProvenanceID:uuid = uuid4()
		provenance:list = [f"ctd:provenance-{ProvenanceID} rdf:type bp3:Provenance .", "bp3:Provenance rdfs:subClassOf bp3:UtilityClass .", f"ctd:provenance-{ProvenanceID} bp3:displayName 'CTD' .", f"ctd:provenance-{ProvenanceID} bp3:standardName 'Comparative Toxicogenomics Database' .", f"ctd:provenance-{ProvenanceID} bp3:name 'ctd base' ."]
		dest_file.write("\n".join(provenance))

		# Description of each line 
		for current_line in tsv_file:
			dest_file.write("\n")
			ChemicalID, ChemicalName, CasRN, GeneID, GeneForms, GeneSymbol, Organism, OrganismID, Interaction, InteractionActions, PubMedIDs = GetVariables(current_line)

			# Describe entities as RDF triples
			# dico_copy is a modified version of the DicoIDs
			dico_entities = DescribeEntities(dest_file = dest_file, dico = DicoIds, provenance = ProvenanceID, \
				ChemicalID = ChemicalID, ChemicalName = ChemicalName, CasRN = CasRN, GeneID = GeneID, GeneForms = GeneForms, \
					 GeneSymbol = GeneSymbol, Organism = Organism, OrganismID = OrganismID)

			# Split InteractionActions into elementary interactions
			InteractionList:list = InteractionActions.split("|")
			# Split textual descriptions of interactions
			InteractionNames, InteractionID, Pmids = split('\[(.*?)\]', Interaction), uuid4(), PubMedIDs.split("|")

			# Describe the chemical-gene interactions
			# dico_copy2 is a second modified version of the DicoIDs
			dico_interactions = DescribeInteractions(current_line = current_line, dest_file = dest_file, ChemicalID = ChemicalID, GeneID = GeneID, provenance = ProvenanceID, GeneSymbol = GeneSymbol, OrganismID = OrganismID, \
					Interaction = Interaction, InteractionInList = InteractionList[0], InteractionList = InteractionList, InteractionID = InteractionID, InteractionNames = InteractionNames, DicoInteractions = DicoIds, PmidsList = Pmids)
			# Unpack and merge the two copies of the dico in the variable DicoIDs
			DicoIds = {**DicoIds,**dico_entities, **dico_interactions}
		# Write entities, interactions and IDs in an output file
		WriteDicoIDs(f"{name_for_output}_DicoIDs.txt", DicoIds)

	# Timer
	EndTime = time()
	print(EndTime - StartTime)
