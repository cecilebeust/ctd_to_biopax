from uuid import uuid4
from copy import deepcopy
import uuid

def DescribeEntities(dest_file, dico:dict, provenance:uuid, ChemicalID:str, ChemicalName:str, CasRN:str, \
    GeneID:str, GeneForms:str, GeneSymbol:str, Organism:str, OrganismID:str)-> dict:
    '''
    Function which describes entities (chemical and gene) involved \
        in a chemical-gene interaction as RDF triples

    dest_file (_io.TextIOWrapper) : name of the Turtle file in which we write RDF triples
    dico (str) : name of the dictionary containing the IDs associated to \
        entities and interactions    
    provenance (uuid) : ID of the provenance (CTD)
    ChemicalID (str) : the ID of the chemical of the current line
    ChemicalName (str) : the name of the chemical
    CasRN (str) : the Cas Registry Number of the chemical
    GeneID (str) : the ID of the gene of the current line
    GeneForms (str) : the product of the gene
    GeneSymbol (str) :  the name of the gene
    Organism (str) : the name of the organism studied
    OrganismID (str) : the ID of the organism

    return (dict) : a dictionary containing entities and their corresponding IDs
    '''

    # Make a copy of the Dico to modify it
    dico_copy:dict = deepcopy(dico)
    # Add entities IDs in dico
    dico_copy[str(ChemicalName)] = f"mesh:{ChemicalID}"

    if ChemicalName in dico_copy:
        SmallMolRefID:uuid = dico_copy[str(ChemicalName)]
    else:
        SmallMolRefID:uuid = uuid4()

    # Store information about the chemical in a list
    if ChemicalID != "":
        chemical:list = [f"mesh:{ChemicalID} rdf:type bp3:SmallMolecule .", "bp3:SmallMolecule rdfs:subClassOf bp3:PhysicalEntity ."]
    if ChemicalID != "" and ChemicalName != "":
        chemical += [
            f"mesh:{ChemicalID} rdfs:label \"{ChemicalName}\" .",
            f"mesh:{ChemicalID} skos:prefLabel \"{ChemicalName}\" .",
            f"mesh:{ChemicalID} bp3:name \"{ChemicalName}\" .",
            f"mesh:{ChemicalID} bp3:entityReference ctd:small-molecule-reference-{SmallMolRefID} .",
            f"ctd:small-molecule-reference-{SmallMolRefID} rdf:type bp3:SmallMoleculeReference .",
            "bp3:SmallMoleculeReference rdfs:subClassOf bp3:EntityReference .",
            f"mesh:{ChemicalID} bp3:dataSource ctd:provenance-{provenance} ."
            ]
    # Add CasRN to the dest file
    if CasRN != "":
        #dest_file.write("mesh:{} bp3:xref [ rdf:type bp3:UnificationXref, bp3:db \"CAS\", bp3:id \"{}\" ] .\n".format(ChemicalID, CasRN))
        chemical += [f"mesh:{ChemicalID} bp3:xref cas:{CasRN} ."]

    # Write the list in the output file
    dest_file.write("\n".join([line for line in chemical]))
    
    # Store information about the Gene in a list
    gene:list = []
    if GeneID != "":
        # TODO: choose the most relevant type
        gene += [
            f"\ngene:{GeneID} rdf:type bp3:Gene .",
            "bp3:Gene rdfs:subClassOf bp3:Entity .",
            f"gene:{GeneID} rdf:type mesh:D005796 . # Gene",
            f"gene:{GeneID} rdf:type so:0000704 . # Gene (http://purl.obolibrary.org/obo/SO_0000704)",
            f"gene:{GeneID} rdf:type sio:010035 . # Gene (http://semanticscience.org/resource/SIO_010035)"]
    if GeneID != "" and GeneSymbol != "":
        # TODO: choose the moste relevant label
        gene += [
            f"gene:{GeneID} bp3:name \"{GeneSymbol}\" .",
            f"gene:{GeneID} rdfs:label \"{GeneSymbol}\" .",
            f"gene:{GeneID} skos:prefLabel \"{GeneSymbol}\"."]
    # add Gene Forms
    if GeneID != "" and GeneForms != "":
        gene += [f"gene:{GeneID} ctd:geneForms \"{GeneForms}\" ."]
    # if it is a protein, add MESH protein ID
    if GeneForms == "protein" and GeneID != "":
        gene += [f"gene:{GeneID} rdf:type mesh:D011506 . # Protein "]
    # Add Organism and OrganismID to the dest file
    if GeneID != "" and Organism != "" :
        gene += [f"gene:{GeneID} bp3:organism taxon:{OrganismID} ."]
    if Organism != "" and OrganismID != "":
        gene += [f"taxon:{OrganismID} bp3:name \"{Organism}\" ."]

    # Write information about the gene in the output file
    dest_file.write("\n".join([line for line in gene]))
    
    return(dico_copy)
