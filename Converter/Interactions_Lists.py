class Interactions:
    """
    Stores the lists containing interactions affected to a BioPAX class
    
    """
    CONTROL:list = ["affects^reaction", "decreases^reaction", "increases^reaction", "decreases^activity", "increases^activity" \
        , "affects^activity", "decreases^export", "decreases^cleavage", "decreases^degradation", "decreases^secretion" \
            , "decreases^localization", "decreases^hydroxylation", "decreases^chemical synthesis", "decreases^nitrosation" \
                , "decreases^lipidation", "decreases^alkylation", "decreases^uptake", "decreases^transport", "decreases^ubiquitination" \
                    , "decreases^ADP-ribosylation", "decreases^prenylation", "decreases^glycosylation", "decreases^sulfation" \
                        , "decreases^ethylation", "decreases^amination", "decreases^import", "decreases^glutathionylation" \
                            , "decreases^folding", "decreases^carboxylation", "decreases^oxidation", "decreases^hydrolysis" \
                                , "decreases^reduction", "decreases^farnesylation", "decreases^splicing", "affects^folding" \
                                    , "affects^glycosylation", "affects^mutagenesis", "affects^glutathionylation", "affects^export" \
                                        , "affects^reduction", "affects^oxidation", "affects^chemical synthesis", "decreases^phosphorylation"\
                                            , "affects^localization", "affects^abundance", "decreases^glucuronidation", "affects^ubiquitination"\
                                                , "decreases^abundance", "affects^transport", "affects^methylation", "decreases^methylation"\
                                                    , "affects^phosphorylation", "affects^secretion", "affects^stability", "decreases^acetylation"\
                                                        , "affects^uptake", "affects^hydroxylation", "affects^glucuronidation", "affects^cleavage"\
                                                            , "affects^splicing", "affects^acetylation", "affects^sulfation", "affects^hydrolysis"\
                                                                , "affects^alkylation", "affects^degradation", "affects^sumoylation", "affects^import"\
                                                                    , "decreases^O-linked glycosylation", "decreases^sumoylation", "decreases^mutagenesis"\
                                                                        , "decreases^geranoylation", "decreases^glycation", "decreases^carbamoylation"\
                                                                            , "affects^farnesylation", "decreases^N-linked glycosylation"\
                                                                                , "decreases^palmitoylation", "affects^carboxylation"]

    TEMPLATEREACTIONREGULATION:list = ["affects^expression", "decreases^expression", "increases^expression"]

    CATALYSIS:list = ["increases^alkylation", "increases^mutagenesis", "increases^ubiquitination", "increases^ribosylation"\
        , "increases^lipidation", "increases^carbamoylation", "increases^sumoylation", "increases^glycosylation"\
            , "increases^glycation", "affects^binding", "increases^folding", "increases^acetylation", "increases^sulfation"\
                , "increases^import", "increases^transport", "increases^chemical synthesis", "decreases^stability"\
                    , "increases^abundance", "increases^hydroxylation", "increases^glucuronidation", "increases^cleavage"\
                        , "increases^export", "increases^phosphorylation", "increases^secretion", "increases^degradation"\
                            , "increases^reduction", "increases^hydrolysis", "increases^oxidation", "increases^localization"\
                                , "increases^splicing", "increases^uptake", "increases^glutathionylation", "increases^nitrosation"\
                                    , "increases^ADP-ribosylation", "increases^folding", "increases^acylation", "increases^O-linked glycosylation"\
                                        , "increases^amination", "increased^prenylation", "increases^N-linked glycosylation", "increases^carboxylation"\
                                            , "increased^farnesylation"]