# Python CTD-to-BioPAX converter

* *Converter_Chemical_Gene_Interactions.py* : python main code of the converter
* *DescribeEntities.py* : file containing a function which describes entities (chemicals and genes) as RDF triples
* *DescribeInteractions.py* : file containing a function which recursively describes interactions between chemicals and genes
* *Utilities.py* : file containing functions used in the main code
The **ConverterExampleChemical_10074-G5** folder contains the files obtained after running the converter on the *CTD_chem_gene_ixns_TEST.tsv* file