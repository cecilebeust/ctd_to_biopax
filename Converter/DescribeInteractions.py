from uuid import uuid4
from copy import deepcopy
import uuid
from Interactions_Lists import Interactions

def DescribeInteractions(current_line:dict, dest_file, ChemicalID:str, GeneID:str, provenance:uuid, GeneSymbol:str, \
    OrganismID:str, Interaction:str, InteractionInList:str, InteractionList:str, InteractionID:uuid, InteractionNames:list, \
    DicoInteractions:dict, PmidsList:list, PreviousInteraction="NoPreviousInteraction")->dict:
        '''
        
        Function which recursively describes successive chained interactions from an interaction list
        
        current_line(dict) : the line of the file we are reading
        dest_file (_io.TextIOWrapper) : name of the Turtle file in which we write RDF triples
        ChemicalID (str) : the ID of the chemical of the current line
        GeneID (str) : the ID of the gene of the current line
        provenance (uuid.UUID) : ID of the provenance (CTD)
        GeneSymbol (str) :  the name of the gene
        OrganismID (str) : the ID of the organism
        Interaction (str) : textual description of the chemical-gene interaction
        InteractionInList (str): The interaction we are describing
        InteractionList (List) : The list containing all the interactions to describe successively
        InteractionID (uuid.UUID) : The ID of the current interaction
        InteractionNames (List) : A list containing the strings describing all the interactions in the interaction list
        DicoInteractions (dict) : Dictionary containing the reactions and entities and their corresponding IDs
        PmidsList (List) : list containing the PubMed identifiers for the current interaction
        PreviousInteraction (str) (default = "NoPreviousInteraction) : Name of the previous described interaction if it exists
        
        return (dict) : a dictionary containing the interactions and their corresponding IDs
        '''
        # Store information about the interaction in a list
        interactions:list = []
        dico_copy2 = deepcopy(DicoInteractions)

        # CONTROL INTERACTIONS
        if InteractionInList in Interactions.CONTROL:
            if PreviousInteraction != "NoPreviousInteraction":
                interactions += [f"\n{PreviousInteraction} bp3:controlled ctd:control-{InteractionID} ."]
            for ReactionName in InteractionNames:
                if InteractionInList.split("^")[1] in ReactionName: # if the term describing the interaction is \
                    # present in the name of the interaction in the list
                    if ReactionName in dico_copy2.keys(): # if the Interaction has already an ID use this ID
                        InteractionID:uuid = dico_copy2[ReactionName] # get the ID of the interaction
                        interactions += [f"ctd:control-{InteractionID} bp3:name \"{ReactionName}\" ."] # make the ID and the reaction name correspond
                    else: # else create a new ID
                        interactions += [f"ctd:control-{InteractionID} bp3:name \"{ReactionName}\" ."]
                        dico_copy2[ReactionName] = InteractionID # add new ID to dico
            interactions += [
                f"ctd:control-{InteractionID} rdf:type bp3:Control .", 	
                "bp3:Control rdfs:subClassOf bp3:Interaction .",
                f"ctd:control-{InteractionID} rdf:resource mesh:{ChemicalID} ." ,
                f"ctd:control-{InteractionID} bp3:controller mesh:{ChemicalID} ." ,
                f"ctd:control-{InteractionID} bp3:controlType \"{InteractionInList}\" ."]
            if "decreases" in InteractionInList:
                interactions += [f"ctd:control-{InteractionID} bp3:controlType 'INHIBITION' ."]
            elif "increases" in InteractionInList:
                interactions += [f"ctd:control-{InteractionID} bp3:controlType 'ACTIVATION' ."]
            interactions += [
                f"ctd:control-{InteractionID} bp3:participant mesh:{ChemicalID} .",
                f"ctd:control-{InteractionID} bp3:participant gene:{GeneID} .",
                f"ctd:control-{InteractionID} bp3:dataSource ctd:provenance-{provenance} ."]

            # Add the pubmed ID of the Interaction
            for pmid in PmidsList:
                interactions += [
                    f"ctd:control-{InteractionID} bp3:xref pubmed:{pmid}.",
                    f"pubmed:{pmid} rdf:type bp3:Xref .",
                    f"bp3:Xref rdfs:subClassOf bp3:UtilityClass ."]
            
            # Write information about the interaction in the output file 
            dest_file.write("\n".join([line for line in interactions]))

            # This interaction is the previous interaction of the next interaction
            PreviousInteraction:str = f"ctd:control-{InteractionID}"

            # If the current interaction is not the last of the list (but need to not have duplicates in the list)
            if InteractionInList != InteractionList[-1]:
                # Attribute an ID to the controllled reaction
                NextInteractionID:uuid = uuid4()
                # Remove the interaction desbribed from the list
                InteractionListReduced:list = InteractionList[1:]
                # Describe the next interaction
                DescribeInteractions(current_line, dest_file, ChemicalID, GeneID, provenance, GeneSymbol, \
                OrganismID, Interaction, InteractionListReduced[0], InteractionListReduced, NextInteractionID, \
                InteractionNames, DicoInteractions, PmidsList, PreviousInteraction)
        
        # TEMPLATE REACTION REGULATION INTERACTIONS
        elif InteractionInList in Interactions.TEMPLATEREACTIONREGULATION:
            if PreviousInteraction != "NoPreviousInteraction":
                interactions += [f"\n{PreviousInteraction} bp3:controlled ctd:template-reaction-regulation-{InteractionID} ."]
            for ReactionName in InteractionNames:
                if InteractionInList.split("^")[1] in ReactionName:
                    if ReactionName in dico_copy2.keys(): 
                        InteractionID:uuid = dico_copy2[ReactionName]
                        interactions += [f"ctd:template-reaction-regulation-{InteractionID} bp3:name \"{ReactionName}\" ."] 
                    else:
                        interactions += [f"ctd:template-reaction-regulation-{InteractionID} bp3:name \"{ReactionName}\" ."]
                        dico_copy2[ReactionName] = InteractionID
            interactions += [
                f"ctd:template-reaction-regulation-{InteractionID} rdf:type bp3:TemplateReactionRegulation .", # Class TemplateReactionRegulation
                f"bp3:TemplateReactionRegulation rdfs:subClassOf bp3:Control ." ,# Subclass of Control
                f"ctd:template-reaction-regulation-{InteractionID} rdf:resource mesh:{ChemicalID} .", # Resource
                f"ctd:template-reaction-regulation-{InteractionID} bp3:controller mesh:{ChemicalID} .", # TF + regulatory element = controller complex
                f"ctd:template-reaction-regulation-{InteractionID} bp3:controlType \"{InteractionInList}\" ."] # controlType or InteractionType ?
            # If the gene expression is decreased, add the inhibition control type 
            if "decreases^expression" in InteractionInList:
                interactions += [f"ctd:template-reaction-regulation-{InteractionID} bp3:controlType 'INHIBITION' ."]
            elif "increases^expression" in InteractionInList:
                interactions += [f"ctd:template-reaction-regulation-{InteractionID} bp3:controlType 'ACTIVATION' ."]
            interactions += [
                f"ctd:template-reaction-regulation-{InteractionID} bp3:participant mesh:{ChemicalID} .",
                f"ctd:template-reaction-regulation-{InteractionID} bp3:participant gene:{GeneID} .",
                f"ctd:template-reaction-regulation-{InteractionID} bp3:dataSource ctd:provenance-{provenance} ."]

            PreviousInteraction:str = f"ctd:template-reaction-regulation-{InteractionID}"
        
            TRRName:str = f"expression of {GeneSymbol} mRNA"
            if TRRName in dico_copy2.keys():
                TemplateReactionID:uuid = dico_copy2[TRRName]
            else:
                TemplateReactionID:uuid = uuid4() # ID of the TemplateReaction regulated
                dico_copy2[TRRName] = TemplateReactionID
            
            interactions += [
                f"ctd:template-reaction-regulation-{InteractionID} bp3:controlled ctd:template-reaction-{TemplateReactionID} . ",
                f"ctd:template-reaction-{TemplateReactionID} bp3:name \"{TRRName}\" .",
                f"ctd:template-reaction-{TemplateReactionID} rdf:type bp3:TemplateReaction .", # Class TemplateReaction
                "bp3:TemplateReaction rdfs:subClassOf bp3:Interaction .", # Subclass of Interaction
                f"ctd:template-reaction-{TemplateReactionID} bp3:template 'DNAregion' .", # The template is a DNA region because it is the case of DNA transcription to RNA
                f"ctd:template-reaction-{TemplateReactionID} bp3:templateDirection 'FORWARD' .",
                f"ctd:template-reaction-{TemplateReactionID} bp3:dataSource ctd:provenance-{provenance} ."]
            
            RnaName:str = str(GeneSymbol) + " mRNA"
            if RnaName in dico_copy2.keys():
                RnaID:uuid = dico_copy2[RnaName]
            else:
                RnaID:uuid = uuid4() # ID of the mRNA producted by the TemplateReaction (=transcription)
                DicoInteractions[RnaName] = RnaID
            
            RnaRefName:str = str(GeneSymbol) + " mRNA reference"
            if RnaRefName in dico_copy2.keys():
                RnaRefID:uuid = dico_copy2[RnaRefName]
            else:
                RnaRefID:uuid = uuid4() # ID of the RnaReference entity
                dico_copy2[RnaRefName] = RnaRefID

            interactions += [
                f"ctd:template-reaction-{TemplateReactionID} bp3:product ctd:rna-{RnaID} .",
                f"ctd:rna-{RnaID} bp3:name \"{RnaName}\" .",
                f"ctd:rna-{RnaID} rdf:type bp3:Rna .",
                "bp3:Rna rdfs:subClassOf bp3:PhysicalEntity .",
                f"ctd:rna-{RnaID} bp3:entityReference ctd:rna-ref-{RnaRefID} .",
                f"ctd:rna-ref-{RnaRefID} bp3:name \"{RnaRefName}\" .",
                f"ctd:rna-ref-{RnaRefID} rdf:type bp3:RnaReference .", # Class RnaReference
                "bp3:RnaReference rdfs:subClassOf bp3:EntityReference .",# Subclass of EntityReference
                f"ctd:rna-{RnaID} bp3:dataSource ctd:provenance-{provenance} .",
                f"ctd:rna-ref-{RnaRefID} bp3:organism taxon:{OrganismID} ."]
            
            # Add the pubmed ID of the TemplateReactionRegulation Interaction
            for pmid in PmidsList:
                interactions += [
                    f"ctd:template-reaction-regulation-{InteractionID} bp3:xref pubmed:{pmid}.",
                    f"pubmed:{pmid} rdf:type bp3:Xref .",
                    "bp3:Xref rdfs:subClassOf bp3:UtilityClass ."]
            
            dest_file.write("\n".join([line for line in interactions]))
            
            if InteractionInList != InteractionList[-1]:
                NextInteractionID:uuid = uuid4()
                InteractionListReduced:list = InteractionList[1:]
                DescribeInteractions(current_line, dest_file, ChemicalID, GeneID, provenance, GeneSymbol, \
                OrganismID, Interaction, InteractionListReduced[0], InteractionListReduced, NextInteractionID, \
                InteractionNames, DicoInteractions, PmidsList, PreviousInteraction)

        elif InteractionInList in Interactions.CATALYSIS:
            for ReactionName in InteractionNames:
                if InteractionInList.split("^")[1] in ReactionName:
                    if ReactionName in dico_copy2.keys(): 
                        InteractionID:uuid = dico_copy2[ReactionName]
                        interactions += [f"ctd:catalysis-{InteractionID} bp3:name \"{ReactionName}\" ."]
                    else:
                        interactions += [f"ctd:catalysis-{InteractionID} bp3:name \"{ReactionName}\" ."]
                        dico_copy2[ReactionName] = InteractionID
                        interactions += [f"ctd:catalysis-{InteractionID} rdf:type bp3:Catalysis .\n"]
            interactions += [
                "bp3:Catalysis rdfs:subClassOf bp3:Control .",
                f"ctd:catalysis-{InteractionID} bp3:controlled gene:{GeneID} . #Protein form",
                f"ctd:catalysis-{InteractionID} bp3:controlType \"{InteractionInList}\" .",
                f"ctd:catalysis-{InteractionID} bp3:participant mesh:{ChemicalID} .",
                f"ctd:catalysis-{InteractionID} bp3:participant gene:{GeneID} .",
                f"ctd:catalysis-{InteractionID} bp3:dataSource ctd:provenance-{provenance} ."]

            for pmid in PmidsList:
                interactions += [
                    f"ctd:catalysis-{InteractionID} bp3:xref pubmed:{pmid}."
                    f"pubmed:{pmid} rdf:type bp3:Xref .",
                    "bp3:Xref rdfs:subClassOf bp3:UtilityClass ."]
            
            dest_file.write("\n".join([line for line in interactions]))

            PreviousInteraction:str = f"ctd:catalysis-{InteractionID}"

            if InteractionInList != InteractionList[-1]:
                NextInteractionID:uuid = uuid4()
                InteractionListReduced:list = InteractionList[1:]
                DescribeInteractions(current_line, dest_file, ChemicalID, GeneID, provenance, GeneSymbol, \
                OrganismID, Interaction, InteractionListReduced[0], InteractionListReduced, NextInteractionID, \
                InteractionNames, DicoInteractions, PmidsList, PreviousInteraction)


        # If the Interaction does not mention 'expression' or 'activity' or 'reaction' it is treated as a SIMPLE INTERACTION
        else:
            # if the PreviousInteraction is different the default value = there is a previous interaction
            if PreviousInteraction != "NoPreviousInteraction":
                interactions += [f"\n{PreviousInteraction} bp3:controlled ctd:interaction-{InteractionID} ."]
            interactions += [
                f"ctd:interaction-{InteractionID} rdf:type bp3:Interaction .",
                "bp3:Interaction rdfs:subClassOf bp3:PhysicalEntity .",
                f"ctd:interaction-{InteractionID} bp3:name \"{Interaction}\" ."]
            if InteractionInList != "":
                interactions += [
                    f"ctd:interaction-{InteractionID} bp3:interactionType \"{InteractionInList}\" .",
                    f"ctd:interaction-{InteractionID} rdf:resource mesh:{ChemicalID} .",
                    f"ctd:interaction-{InteractionID} bp3:participant mesh:{ChemicalID} .",
                    f"ctd:interaction-{InteractionID} bp3:participant gene:{GeneID} ."]
            interactions += [f"ctd:interaction-{InteractionID} bp3:dataSource ctd:provenance-{provenance} ."]

            for pmid in PmidsList:
                interactions += [
                    f"ctd:interaction-{InteractionID} bp3:xref pubmed:{pmid}.",
                    f"pubmed:{pmid} rdf:type bp3:Xref .",
                    "bp3:Xref rdfs:subClassOf bp3:UtilityClass ."]

            dest_file.write("\n".join([line for line in interactions]))
            
            PreviousInteraction:str = f"ctd:interaction-{InteractionID}"

            if InteractionInList != InteractionList[-1]:
                NextInteractionID:uuid = uuid4()
                InteractionListReduced:list = InteractionList[1:]
                DescribeInteractions(current_line, dest_file, ChemicalID, GeneID, provenance, GeneSymbol, \
                OrganismID, Interaction,InteractionListReduced[0], InteractionListReduced, NextInteractionID, \
                InteractionNames, DicoInteractions, PmidsList, PreviousInteraction)

        return(dico_copy2)