# Example of use of the converter

Here is an example of use of the converter for the conversion of 15 chemical-gene interactions involving the chemical "10074-G5".

* *CTD_chem_gene_ixns_TEST.ttl* : output file describing these 15 interactions in BioPAX
* *CTD_chem_gene_ixns_TEST_DicoIDs.txt* : file listing the unique identifiers attributed to entites and interactions