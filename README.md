# CTD to Biopax Python converter 

This is a python converter which converts chemical-gene interactions from the <markk>Comparative Toxicogenomics Database</mark> (http://ctdbase.org/) into BioPAX (Biological Pathway Exchange, http://www.biopax.org/). This project is inspired by the work of Igor Rodchenkov available on his GitHub repository (https://github.com/PathwayCommons/ctd-to-biopax).

### Structure of the project

* **Converter** : contains the python scripts of the converter + an example of conversion for a set of 15 chemical-gene interactions.
* **Count_Interaction_Actions** : contains the count files for the InteractionActions column of the input file (count the number of chemical-gene interactions to describe). The *CountElementaryInteractionsWODups.txt* file lists all the elementary chemcial-gene interactions mentioned in the CTD file. 
* *ClassAssignments.tsv* : file listing the BioPAX classes attributed to the elementary chemical-gene interactions described in the BioPAX file

### Input 
* *CTD_chem_gene_ixns.tsv* : chemical-gene interactions file downloaded from the CTD

### Usage

* To execute the programm on all the CTD chemical-gene interactions file, download the *CTD_chem_gene_ixns.tsv* file of the CTD (http://ctdbase.org/downloads/#cg) and then :

```
cd Converter/
python Converter_Chemical_Gene_Interactions.py CTD_chem_gene_ixns.tsv
```
* To execute the programm on the example of 15 chemical-gene interactions do:

```
cd Converter/
python Converter_Chemical_Gene_Interactions.py CTD_chem_gene_ixns_TEST.tsv
```

### Outputs  
* *CTD_chem_gene_ixns.ttl* : a BioPAX file describing these chemical-gene interactions in a RDF formalism
* *CTD_chem_gene_ixns_DicoIDs.txt* : a file listing all entities (chemicals, genes) and interactions with their attributed unique identifier in the BioPAX file

### Example of chemical-gene interaction described in BioPAX and represented as an oriented graph

![](DecreasesActivity.png)

Programm to build the graph : https://gitlab.com/odameron/rdf2image

## To improve
* Add Modulation instances : a Control interaction in which a physical entity modulates a catalysis interaction
* Improve textual description of interactions : some of them are not correctly translated in the output file 