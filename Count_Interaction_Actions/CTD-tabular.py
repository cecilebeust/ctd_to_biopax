def ListElementaryInteractions(filename, output):
    '''
    Function which lists the elementary interactions from a tsv file containing the InteractionActions column of the CTD

    filename : an input file containing the InteractionActions column of the CTD where elementary interactions \
        are separated by a pipe
    output : file in which are independantly listed the elementary interactions
    '''
    with open(filename, 'r') as fh:
        with open(output, 'w') as fo:
            for line in fh:
                InteractionsList = line.rsplit("|")
                for interaction in InteractionsList:
                    fo.write(interaction + "\n")

#ListElementaryInteractions("InteractionActions_CTD_chem_gene.tsv", "CountElementaryInteractions.txt")

def RemoveEmptyLines(filename):
    with open(filename, 'r') as fh:
        lines = fh.readlines()
    
    with open(filename, 'w') as fh:
        lines = filter(lambda x: x.strip(), lines) # Remove empty lines
        fh.writelines(lines)

#RemoveEmptyLines("CountElementaryInteractions.txt")

def RemoveDuplicateLines(infilename, outfilename):
    '''
    Function which rewrites the lines of an input file in single copies in an output file
    '''
    lines_seen = set()
    with open(infilename, 'r') as fh:
        with open(outfilename, 'w') as fo:
            for line in fh:
                if line not in lines_seen: # if the current line is not duplcated in the file
                    fo.write(line)
                    lines_seen.add(line)

#RemoveDuplicateLines("CountElementaryInteractions.txt", "CountElementaryInteractionsWODups.txt")

# On pubmed id column

ListElementaryInteractions("PubMedIDs.txt","CountPubMedIDs.txt")
RemoveEmptyLines("CountPubMedIDs.txt")
RemoveDuplicateLines("CountPubMedIDs.txt", "CountPubMedIDsWODups.txt")