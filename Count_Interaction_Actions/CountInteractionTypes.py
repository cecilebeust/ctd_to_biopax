
def CountInteractionTypes(filename, outputfile):
    with open(filename, 'r') as fh:
        DecreasesExpression = 0
        IncreasesExpression = 0
        DecreasesActivity = 0
        IncreasesActivity = 0
        for line in fh:
            if not line.startswith('#'):
                InteractionAction = line.split("\t")[9]
                if  InteractionAction == "decreases^expression":
                    DecreasesExpression += 1
                if  InteractionAction == "increases^expression":
                    IncreasesExpression += 1
                if  InteractionAction == "decreases^activity":
                    DecreasesActivity += 1
                if  InteractionAction == "increases^activity":
                    IncreasesActivity += 1
        with open(outputfile, 'w') as fo:
            fo.write("Number of simple interactions in the file " + str(filename) + "\n")
            fo.write("\n")
            fo.write("decreases^expression" + ":" + str(DecreasesExpression) + "\n")
            fo.write("increases^expression" + ":" + str(IncreasesExpression) + "\n")
            fo.write("decreases^activity" + ":" + str(DecreasesActivity) + "\n")
            fo.write("increases^activity" + ":" + str(IncreasesActivity) + "\n")

CountInteractionTypes("CTD_chem_gene_ixns.tsv", "CountNumberSimpleInteractions.txt")