

def CountInteractions(filename):
    with open(filename, 'r') as fh:
        number_interactions = 0
        for line in fh.readlines():
            number_interactions += int(line.split()[0])
        print(number_interactions)


CountInteractions("InteractionActionsSortedCount.txt")