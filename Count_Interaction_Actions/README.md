# Count the number of chemical-gene interactions in the CTD file

* *CTD-tabular.py* : python script containing functions to list the elementary interactions from the CTD chemical-gene interactions file
* *CountElementaryInteractionsWODups.txt* : file listing the 134 elementary interactions from the CTD file
* *CountInteractionTypes.py* : python script which counts the number of simple reactions of type increases/decreases expression/activity in the CTD file
* *CountNumberSimpleInteractions.txt* : output file of the *CountInteractionTypes.py* script
* *CountPubMedIDsWODups.txt* : file listing all the PubMed identifiers mentioned in the CTD file
* *InteractionActionsSortedCount.txt* : count file which lists and counts the different types of interactions combinations from the CTD file
* *CountInteractions.py* : python script to generate the *InteractionActionSortedCount.txt* file